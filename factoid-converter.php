<?php
/**
* Plugin Name: Factoid Converter
* Plugin URI: https://canonical-ledgers.com
* Description: Adds options to sell entry credits for USD
* Author: Ryan Stonebraker
* Author URI: https://canonical-ledgers.com
* Version: 0.1.0
*/


defined( 'ABSPATH' ) or die( 'Outside execution not allowed!' );

function factoid_converter_init () {
    wp_register_style( 'factoid_converter_option_styles', plugins_url( 'css/options.css', __FILE__ ) );
    add_option( 'factoid_address', '');
    register_setting( 'factoid-converter-config', 'factoid_address', 'verify_factoid_address' );

    add_option( 'factom_cli_path', '');
    register_setting( 'factoid-converter-config', 'factom_cli_path', 'verify_factom_cli_path' );

    add_option( 'factomd_endpoint', '');
    register_setting( 'factoid-converter-config', 'factomd_endpoint', 'verify_factomd_endpoint' );

    add_option( 'factomd_username', '');
    register_setting( 'factoid-converter-config', 'factomd_username', 'sanitize_factomd_username' );

    add_option( 'factomd_password', '');
    register_setting( 'factoid-converter-config', 'factomd_password', 'sanitize_factomd_password' );

    add_option( 'factom_walletd_endpoint', '');
    register_setting( 'factoid-converter-config', 'factom_walletd_endpoint', 'verify_factom_walletd_endpoint' );

    add_option( 'factom_walletd_username', '');
    register_setting( 'factoid-converter-config', 'factom_walletd_username', 'sanitize_factom_walletd_username' );

    add_option( 'factom_walletd_password', '');
    register_setting( 'factoid-converter-config', 'factom_walletd_password', 'sanitize_factom_walletd_password' );

}
add_action( 'admin_init', 'factoid_converter_init' );

function sanitize_factomd_username ($username) {
    return sanitizeField ($username, "factomd_username");
}

function sanitize_factomd_password ($password) {
    return sanitizeField ($password, "factomd_password");
}

function sanitize_factom_walletd_username ($username) {
    return sanitizeField ($username, "factom_walletd_username");
}

function sanitize_factom_walletd_password ($password) {
    return sanitizeField ($password, "factom_walletd_password");
}

function sanitizeField ($userInput, $field) {
    $verified = !preg_match('/[\'"]+/', $userInput);
    return $verified ? $userInput : get_option($field);
}

function verify_factoid_address ($factoid_address) {
    $verified = preg_match('/^(fa|FA)[1-9ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{50}$/', $factoid_address);
    return $verified ? $factoid_address : get_option('factoid_address');
}

function verify_factom_cli_path ($path) {
    // removes all quotes and trailing slashes before validation
    $path = preg_replace('/[\'"]|\/$/', "", $path);
    $verified = preg_match('/^[a-zA-Z0-9\/\\_ -]+$/', $path);
    if ($verified && strpos($path, " "))
        $path = "'$path'";

    return $verified ? $path : get_option('factom_cli_path');
}

function verify_factomd_endpoint ($endpoint) {
    return verify_endpoint($endpoint, 'factomd_endpoint');
}

function verify_factom_walletd_endpoint ($endpoint) {
    return verify_endpoint($endpoint, 'factom_walletd_endpoint');
}

function verify_endpoint ($endpoint, $option) {
    $verified = !preg_match('/[; \t]+/', $endpoint);
    return $verified ? $endpoint : get_option($option);
}

function factoid_converter_add_menus() {
    $main_page = add_menu_page("Factoid Converter Configuration Options", "FactoidConverter", "manage_options", "factoid-converter-options", "factoid_converter_display_main_settings", "", 26);
    add_action( "admin_print_styles-{$main_page}", 'load_main_page_styles' );
}
add_action( 'admin_menu', 'factoid_converter_add_menus' );

function load_main_page_styles () {
    wp_enqueue_style("factoid_converter_option_styles");
}

function factoid_converter_display_main_settings() {
    if (!current_user_can('manage_options')) {
        return;
    }
    ?>
    <div class="wrap">
        <h1><?= esc_html(get_admin_page_title()); ?></h1>
        <form action="options.php" method="post">
            <?php settings_fields( 'factoid-converter-config' ); ?>
            <section class="factoid-address-balance">
                <label for="factoid_address">Factoid Address:</label>
                <input type="text" id="factoid_address" name="factoid_address" value="<?php echo get_option('factoid_address'); ?>" />
                <input type="text" id="factoid-balance" value="Balance: <?php echo get_factoid_balance(); ?>" readonly>
            </section>
            <section>
                <label for="factom_cli_path">Factom CLI Path:</label>
                <input type="text" id="factom_cli_path" name="factom_cli_path" value="<?php echo get_option('factom_cli_path'); ?>" />
            </section>
            <section>
                <label for="factomd_endpoint">Factomd Endpoint:</label>
                <input type="text" id="factomd_endpoint" name="factomd_endpoint" value="<?php echo get_option('factomd_endpoint'); ?>" />
            </section>
            <section>
                <label for="factomd_username">Factomd Username:</label>
                <input type="text" id="factomd_username" name="factomd_username" value="<?php echo get_option('factomd_username'); ?>" />
            </section>
            <section>
                <label for="factomd_password">Factomd Password:</label>
                <input type="text" id="factomd_password" name="factomd_password" value="<?php echo get_option('factomd_password'); ?>" />
            </section>
            <section>
                <label for="factom_walletd_endpoint">Factom-Walletd Endpoint:</label>
                <input type="text" id="factom_walletd_endpoint" name="factom_walletd_endpoint" value="<?php echo get_option('factom_walletd_endpoint'); ?>" />
            </section>
            <section>
                <label for="factom_walletd_username">Factom-Walletd Username:</label>
                <input type="text" id="factom_walletd_username" name="factom_walletd_username" value="<?php echo get_option('factom_walletd_username'); ?>" />
            </section>
            <section>
                <label for="factom_walletd_password">Factom-Walletd Password:</label>
                <input type="text" id="factom_walletd_password" name="factom_walletd_password" value="<?php echo get_option('factom_walletd_password'); ?>" />
            </section>
            <?php submit_button('Save Settings'); ?>
        </form>
    </div>
    <?php
}

function getFactomCLIPreString () {
    $factom_cli_path = get_option("factom_cli_path");
    if (strlen($factom_cli_path) == 0)
        return "";

    $endpoints = "";
    $factomd_endpoint = get_option("factomd_endpoint");
    $endpoints .= $factomd_endpoint != "" ? "-s $factomd_endpoint" : "";

    $factom_walletd_endpoint = get_option("factom_walletd_endpoint");
    $endpoints .= $factom_walletd_endpoint != "" ? " -w $factom_walletd_endpoint" : "";

    $userPass = "";
    $factomd_username = get_option("factomd_username");
    $factomd_password = get_option("factomd_password");
    if (strlen($factomd_username) > 0 && strlen($factomd_password) > 0)
        $userPass .= "-factomduser='$factomd_username' -factomdpassword='$factomd_password' ";
    $factom_walletd_username = get_option("factom_walletd_username");
    $factom_walletd_password = get_option("factom_walletd_password");
    if (strlen($factom_walletd_username) > 0 && strlen($factom_walletd_password) > 0)
        $userPass .= "-walletuser='$factom_walletd_username' -walletpassword='$factom_walletd_password' ";

    return "$factom_cli_path $userPass $endpoints";
}

function get_factoid_balance() {
    $factoid_address = get_option('factoid_address');

    $factomcli = getFactomCLIPreString();
    if (strlen($factoid_address) === 52 && strlen($factomcli) > 0) {
         return shell_exec("$factomcli balance {$factoid_address} 2>&1");
     }
    return "N/A";
}

function factoid_converter_balance() {
    if (!current_user_can('manage_options')) {
        return;
    }
    ?>
    <div class="wrap">
        <h1><?= esc_html(get_admin_page_title()); ?></h1>
        <form action="options.php" method="post">
            <?php
            submit_button('Save Settings');
            ?>
        </form>
    </div>
    <?php
}

// Checks to make sure the entry credit field is right length (52 characters),
// only contains letters and numbers, no i's or o's and starts with "ec" or "EC"
function verifyEntryCreditAddress ($user_input) {
    $verified = preg_match('/^(ec|EC)[1-9ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{50}$/', $user_input);
    return $verified === 1;
}

function buyEntryCredits($entrycredit_address, $amount)
{
    if (!verifyEntryCreditAddress($entrycredit_address))
        return false;

    $factoid_address = get_option("factoid_address");

    $amount = 10;

    $factomcli = getFactomCLIPreString();
    $output = shell_exec("$factomcli buyec -f {$factoid_address} {$entrycredit_address} {$amount} 2>&1");
    return $output;
}

function fulfill_entry_credit_order ($order_id) {
    $order = wc_get_order($order_id);

    // order was already handled
    if ($order->get_status() === 'completed' && metadata_exists('post', $order_id, 'ec_txid'))
        return false;

    $entry_credit_address = get_post_meta($order_id , 'entry_credit_address', true);

    $total_amount = 0;
    foreach ($order->get_items() as $item_key => $item_values):
        $product_id = $item_values->get_product_id();
        $res = get_post_meta($product_id);
        $entry_credit_amount = unserialize($res['_product_attributes'][0])['entrycredits']['value'];
        $total_amount += $entry_credit_amount;
    endforeach;

    $ec_txid = buyEntryCredits($entry_credit_address, $total_amount);
    $ec_txid = str_replace("TxID: ", "", $ec_txid);
    add_post_meta($order_id, 'ec_txid', $ec_txid);
    return true;
}

function entrycredits_woocommerce_thankyou($order_id) {
    if (!$order_id)
        return;

    // WARNING: THIS SHOULD ONLY BE ENABLED FOR LOCALHOST TESTING WHERE IT IS IMPOSSIBLE TO GET PAYPAL IPN
    // ON ACTUAL SERVER THIS SHOULD BE HANDLED USING woocommerce_payment_complete HOOK
    $order_fulfilled = fulfill_entry_credit_order($order_id);
    if ($order_fulfilled === true) {
        wc_get_order($order_id)->update_status('completed');
    }
    $txid = get_post_meta( $order_id , 'ec_txid', true);
    echo "<section class='ec_txid'>Factom TxID: <a href='https://explorer.factom.com/transactions/$txid'>$txid</a><a href='https://explorer.factom.com/transactions/$txid'><i class='fas fa-link'></i></a></section>";
}
add_action( 'woocommerce_thankyou', 'entrycredits_woocommerce_thankyou', 10, 1 );

function entrycredits_woocommerce_payment_complete ($order_id) {
    fulfill_entry_credit_order($order_id);
}
add_action( 'woocommerce_payment_complete', 'entrycredits_woocommerce_payment_complete', 10, 1 );


function entrycredits_woocommerce_ec_address($checkout)
{
	echo '<div id="entrycredits_woocommerce_ec_address"><h2>' . __('Options') . '</h2>';
	woocommerce_form_field('entry_credit_address', array(
		'type' => 'text',
		'class' => array(
			'entry-credit-address form-row-wide'
		) ,
		'label' => __('Entry Credit Address') ,
		'placeholder' => __('Enter your entry credit address') ,
		'required' => true,
	) , $checkout->get_value('entry_credit_address'));
	echo '</div>';
}
add_action('woocommerce_after_order_notes', 'entrycredits_woocommerce_ec_address');


function verify_checkout_fields()
{
	if (!$_POST['entry_credit_address'] || !verifyEntryCreditAddress($_POST['entry_credit_address']))
        wc_add_notice(__('Please enter a valid entry credit address.') , 'error');
}
add_action('woocommerce_checkout_process', 'verify_checkout_fields');


function update_checkout_fields_meta($order_id)
{
	if (!empty($_POST['entry_credit_address'])) {
		update_post_meta($order_id, 'entry_credit_address', sanitize_text_field($_POST['entry_credit_address']));
	}
}
add_action('woocommerce_checkout_update_order_meta', 'update_checkout_fields_meta');


// Remove billing information from checkout
function custom_override_checkout_fields( $fields ) {
    unset($fields['billing']['billing_first_name']);
    unset($fields['billing']['billing_last_name']);
    unset($fields['billing']['billing_company']);
    unset($fields['billing']['billing_address_1']);
    unset($fields['billing']['billing_address_2']);
    unset($fields['billing']['billing_city']);
    unset($fields['billing']['billing_postcode']);
    unset($fields['billing']['billing_country']);
    unset($fields['billing']['billing_state']);
    unset($fields['billing']['billing_phone']);
    unset($fields['order']['order_comments']);
    unset($fields['billing']['billing_address_2']);
    unset($fields['billing']['billing_postcode']);
    unset($fields['billing']['billing_company']);
    unset($fields['billing']['billing_email']);
    unset($fields['billing']['billing_city']);
    return $fields;
}
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
